<?php

namespace App\Domain\Entity;

use App\Domain\Exception\ValidationException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Uid\Uuid;

class Team
{
    private Uuid $id;

    private string $name;

    private Collection $players;

    /**
     * @throws ValidationException
     */
    public function __construct(Uuid $id, string $name)
    {
        $this->id = $id;

        if (mb_strlen($name) > 255 || empty($name)) {
            throw new ValidationException("Each team must have a name with less than 255 characters.");
        }

        $this->name = $name;
        $this->players = new ArrayCollection();
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    /**
     * @param Player $player
     * @return $this
     */
    public function addPlayer(Player $player) : self
    {
        $player->setTeam($this);
        $this->players->add($player);

        return $this;
    }

    /**
     * @param Player $player
     * @return $this
     */
    public function removePlayer(Player $player) : self
    {
        if(!$this->players->contains($player)) {
            return $this;
        }
        $player->setTeam(null);
        $this->players->removeElement($player);

        return $this;
    }


}