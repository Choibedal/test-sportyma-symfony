<?php

namespace App\Domain\Entity;

use App\Domain\Exception\ValidationException;
use Symfony\Component\Uid\Uuid;

class SportsMatch
{
    private Uuid $id;

    private string $name;
    private ?Team $hostTeam;

    private ?Team $visitorTeam;

    /**
     * @throws ValidationException
     */
    public function __construct(Uuid $id, string $name, Team $hostTeam, Team $visitorTeam)
    {
        $this->id = $id;
        if (mb_strlen($name) > 255) {
            throw new ValidationException("Name must have less than 255 characters.");
        }
        $this->name = $name;
        $this->hostTeam = $hostTeam;
        $this->visitorTeam = $visitorTeam;
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param ?Team $team
     * @return $this
     */
    public function setHostTeam(?Team $team) : self
    {
        $this->hostTeam = $team;
        return $this;
    }

    /**
     * @return Team|null
     */
    public function getHostTeam(): ?Team
    {
        return $this->hostTeam;
    }
    /**
     * @param ?Team $team
     * @return $this
     */
    public function setVisitorTeam(?Team $team) : self
    {
        $this->visitorTeam = $team;
        return $this;
    }

    /**
     * @return Team|null
     */
    public function getVisitorTeam(): ?Team
    {
        return $this->visitorTeam;
    }
}