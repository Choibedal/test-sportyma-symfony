<?php

namespace App\Domain\Repository;

use App\Domain\Entity\SportsMatch;
use App\Domain\Entity\Team;

interface SportsMatchRepository
{
    public function create(SportsMatch $match): void;

    /**
     * @return array<SportsMatch>
     */
    public function findAll(): array;

    /**
     * @return array<SportsMatch>
     */
    public function findByTeam(Team $team) : array;


}