<?php

namespace App\Infrastructure\Symfony\Command;

use App\UseCase;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: self::COMMAND_NAME,
    description: 'Create a match.',
    hidden: false
)]
final class CreateMatchCommand extends Command
{
    public const COMMAND_NAME = "app:match:create";
    public const ARGUMENT_MATCH_NAME = 'match-name';
    public const ARGUMENT_HOST_TEAM_ID = 'host-team-id';
    public const ARGUMENT_VISITOR_TEAM_ID = 'visitor-team-id';


    public function __construct(
        private readonly UseCase\GetTeam\UseCase $getTeamUC,
        private readonly UseCase\CreateMatch\UseCase $createMatchUC,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_MATCH_NAME, InputArgument::REQUIRED, 'The name of the match.');
        $this->addArgument(self::ARGUMENT_HOST_TEAM_ID, InputArgument::REQUIRED, 'The id of the host team');
        $this->addArgument(self::ARGUMENT_VISITOR_TEAM_ID, InputArgument::REQUIRED, 'The id of the visitor team');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);

        $matchName = $input->getArgument((self::ARGUMENT_MATCH_NAME));
        $hostTeamIdInput = $input->getArgument((self::ARGUMENT_HOST_TEAM_ID));
        $visitorTeamIdInput = $input->getArgument((self::ARGUMENT_VISITOR_TEAM_ID));

        try {
            $hostTeamResponse = $this->getTeamUC->execute(new UseCase\GetTeam\Request($hostTeamIdInput));
            $hostTeam = $hostTeamResponse->getTeam();
            if(null === $hostTeam) {
                $io->error("La team à l'id" . $hostTeamIdInput . " est introuvable.");
                return Command::FAILURE;
            }

            $visitorTeamResponse = $this->getTeamUC->execute(new UseCase\GetTeam\Request($visitorTeamIdInput));
            $visitorTeam = $visitorTeamResponse->getTeam();
            if(null === $visitorTeam) {
                $io->error("La team à l'id" . $hostTeamIdInput . " est introuvable.");
                return Command::FAILURE;
            }


            $match = $this->createMatchUC->execute(new UseCase\CreateMatch\Request($matchName, $hostTeam, $visitorTeam))->getMatch();

        } catch (\Exception $error) {
            $io->error($error->getMessage());
            return Command::FAILURE;
        }
        $io->success("Le match " . $match->getName() . " est prévue entre l'équipe " . $hostTeam->getName() . " et " . $visitorTeam->getName());
        return Command::SUCCESS;

    }
}