<?php

namespace App\Infrastructure\Symfony\Command;

use App\UseCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: self::COMMAND_NAME,
    description: 'Add a player by name to a team by name.',
    hidden: false
)]
final class AddPlayerToTeamCommand extends Command
{
    public const COMMAND_NAME = "app:player:add-to-team";
    public const ARGUMENT_PLAYER_ID = 'player-id';
    public const ARGUMENT_TEAM_ID = 'team-id';


    public function __construct(
        private readonly UseCase\GetPlayer\UseCase $getPlayerUC,
        private readonly UseCase\GetTeam\UseCase $getTeamUC,
        private readonly UseCase\AddPlayerToTeam\UseCase $addPlayerToTeamUC,
        private readonly EntityManagerInterface $entityManager
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(self::ARGUMENT_PLAYER_ID, InputArgument::REQUIRED, 'The id of the player.');
        $this->addArgument(self::ARGUMENT_TEAM_ID, InputArgument::REQUIRED, 'The id of the team.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $playerIdInput = $input->getArgument((self::ARGUMENT_PLAYER_ID));
        $teamIdInput = $input->getArgument((self::ARGUMENT_TEAM_ID));

        try {
            $playerResponse = $this->getPlayerUC->execute(new UseCase\GetPlayer\Request($playerIdInput));
            $player = $playerResponse->getPlayer();

            if(null === $player) {
                $io->error("Le joueur à l'id " . $playerIdInput . " est introuvable.");
                return Command::FAILURE;
            }

            $teamResponse = $this->getTeamUC->execute(new UseCase\GetTeam\Request($teamIdInput));
            $team = $teamResponse->getTeam();

            if(null === $team) {
                $io->error("La team à l'id" . $teamIdInput . " est introuvable.");
                return Command::FAILURE;
            }

            // Make sure the team doesn't already contain the player and check remove function works
            $team->removePlayer($player);
            $this->addPlayerToTeamUC->execute(new UseCase\AddPlayerToTeam\Request($player, $team));
            if(!$team->getPlayers()->contains($player)) {
                $io->error("Le joueur n'a pas pu être ajouté à l'équipe");
                return Command::FAILURE;
            }

            $this->entityManager->flush();

        } catch (\Exception $error) {
            $io->error($error->getMessage());
            return Command::FAILURE;
        }
        $io->success("Le joueur " . $player->getName() . " fait désormais partie de l'équipe " . $team->getName());
        return Command::SUCCESS;

    }
}