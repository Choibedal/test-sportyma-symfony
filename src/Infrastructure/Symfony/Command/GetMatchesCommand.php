<?php

namespace App\Infrastructure\Symfony\Command;

use App\UseCase;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: self::COMMAND_NAME,
    description: 'Display teams.',
    hidden: false
)]
final class GetMatchesCommand extends Command
{
    public const COMMAND_NAME = "app:match:list";
    public const ARGUMENT_TEAM_ID = 'team-id';
    public function __construct(
        private readonly UseCase\GetTeam\UseCase $getTeamUC,
        private readonly UseCase\GetMatches\UseCase $getMatchesUC
    )
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument(self::ARGUMENT_TEAM_ID, InputArgument::OPTIONAL, 'The ID of the team.');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $team = null;
        $teamId = $input->getArgument(self::ARGUMENT_TEAM_ID);
        if(!empty($teamId)) {
            $team = $this->getTeamUC->execute(new UseCase\GetTeam\Request($teamId))->getTeam();
        }

        $response = $this->getMatchesUC->execute(new UseCase\GetMatches\Request($team));

        $table = new Table($output);
        $rows = array();
        foreach ($response->getMatches() as $match) {
            $rows[] = array($match->getId(), $match->getName());
        }
        $table
            ->setHeaders(['Id', 'Match name'])
            ->setRows($rows);
        $table->render();
        return Command::SUCCESS;
    }
}