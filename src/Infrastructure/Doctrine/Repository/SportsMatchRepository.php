<?php

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Entity\SportsMatch;
use App\Domain\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SportsMatchRepository extends ServiceEntityRepository implements \App\Domain\Repository\SportsMatchRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SportsMatch::class);
    }

    public function create(SportsMatch $match): void
    {
        $this->_em->persist($match);
        $this->_em->flush();
    }

    /**
     * @return SportsMatch[]
     */
    public function findAll(): array
    {
        return $this->findBy(array());
    }

    public function findByTeam(?Team $team): array
    {

        return $this->createQueryBuilder("sm")
            ->where('sm.hostTeam = :team')
            ->orWhere('sm.visitorTeam = :team')
            ->setParameter("team", $team)
            ->getQuery()->getResult();
        // TODO: Implement findByTeamId() method.
    }
}