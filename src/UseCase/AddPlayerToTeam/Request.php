<?php

namespace App\UseCase\AddPlayerToTeam;

use App\Domain\Entity\Player;
use App\Domain\Entity\Team;

final class Request
{
    public function __construct(private readonly Player $player, private readonly Team $team)
    {
    }

    public function getPlayer(): Player
    {
        return $this->player;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }
}