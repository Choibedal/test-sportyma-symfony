<?php

namespace App\UseCase\AddPlayerToTeam;

use App\Domain\Entity\Player;
use App\Domain\Entity\Team;

class Response
{
    public function __construct(private readonly Player $player, private readonly Team $team)
    {
    }


    public function getPlayer(): PLayer
    {
        return $this->player;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }
}