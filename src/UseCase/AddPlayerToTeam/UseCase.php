<?php

namespace App\UseCase\AddPlayerToTeam;

class UseCase
{
    public function __construct()
    {
    }

    public function execute(Request $request): Response
    {
        $player = $request->getPlayer();
        $team = $request->getTeam();

        if(!$team->getPlayers()->contains($player)) {
            $team->addPlayer($player);
        }

        return new Response($player, $team);
    }
}