<?php

namespace App\UseCase\GetPlayer;

final class Request
{
    public function __construct(private readonly string $id)
    {
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}