<?php

namespace App\UseCase\GetPlayer;

use App\Domain\Entity\Player;

final class Response
{
    public function __construct(private readonly Player $player)
    {
    }

    /**
     * @return Player|null
     */
    public function getPlayer(): ?Player
    {
        return $this->player;
    }
}