<?php

namespace App\UseCase\CreateMatch;

use App\Domain\Entity\SportsMatch;

class Response
{
    public function __construct(private readonly SportsMatch $match)
    {
    }


    public function getMatch(): SportsMatch
    {
        return $this->match;
    }

}