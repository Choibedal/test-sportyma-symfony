<?php

namespace App\UseCase\CreateMatch;

use App\Domain\Entity\Team;

final class Request
{
    public function __construct(private readonly string $name, private readonly Team $hostTeam, private readonly Team $visitorTeam)
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Team|null
     */
    public function getHostTeam(): ?Team
    {
        return $this->hostTeam;
    }

    /**
     * @return Team|null
     */
    public function getVisitorTeam(): ?Team
    {
        return $this->visitorTeam;
    }
}
