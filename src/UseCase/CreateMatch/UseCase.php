<?php

namespace App\UseCase\CreateMatch;

use App\Domain\Entity\SportsMatch;
use App\Domain\Repository\SportsMatchRepository;
use Symfony\Component\Uid\Uuid;

class UseCase
{
    public function __construct(private readonly SportsMatchRepository $sportsMatchRepository)
    {
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \App\Domain\Exception\ValidationException
     */
    public function execute(Request $request): Response
    {
        $name = $request->getName();
        $host = $request->getHostTeam();
        $visitor = $request->getVisitorTeam();

        $this->sportsMatchRepository->create($match = (new SportsMatch(Uuid::v4(), $name, $host, $visitor)));

        return new Response($match);
    }
}