<?php

namespace App\UseCase\GetTeam;


use App\Domain\Entity\Team;

final class Response
{
    public function __construct(private readonly Team $team)
    {
    }

    /**
     * @return Team|null
     */
    public function getTeam(): ?Team
    {
        return $this->team;
    }
}