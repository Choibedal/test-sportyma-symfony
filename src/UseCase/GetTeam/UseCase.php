<?php

namespace App\UseCase\GetTeam;

use App\Domain\Repository\TeamRepository;

final class UseCase
{
    public function __construct(private readonly TeamRepository $teamRepository)
    {
    }

    public function execute(Request $request): Response
    {
        return new Response($this->teamRepository->findOneBy(["id" => $request->getId()]));
    }
}