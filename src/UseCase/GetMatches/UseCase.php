<?php

namespace App\UseCase\GetMatches;

use App\Domain\Repository\SportsMatchRepository;

final class UseCase
{
    public function __construct(private readonly SportsMatchRepository $sportsMatchRepository)
    {
    }

    public function execute(Request $request): Response
    {

        if (empty($request->getTeam())) {
            return new Response($this->sportsMatchRepository->findAll());
        }

        return new Response($this->sportsMatchRepository->findByTeam($request->getTeam()));

    }
}