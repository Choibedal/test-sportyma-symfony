<?php

namespace App\UseCase\GetMatches;

use App\Domain\Entity\SportsMatch;

final class Response
{
    public function __construct(private readonly array $matches)
    {
    }

    /**
     * @return array<SportsMatch>
     */
    public function getMatches(): array
    {
        return $this->matches;
    }
}