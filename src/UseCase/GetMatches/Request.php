<?php

namespace App\UseCase\GetMatches;

use App\Domain\Entity\Team;

final class Request
{
    public function __construct(private readonly ?Team $team)
    {
    }

    /**
     * @return Team|null
     */
    public function getTeam(): ?Team
    {
        return $this->team;
    }
}