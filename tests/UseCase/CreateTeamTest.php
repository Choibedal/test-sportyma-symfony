<?php

namespace App\Tests\UseCase;

use App\Infrastructure\Symfony\Command\CreateTeamCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class CreateTeamTest extends KernelTestCase
{
    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find(CreateTeamCommand::COMMAND_NAME);
        $commandTester = new CommandTester($command);

        $name = "team-" . (new \DateTime())->format("cu");
        $r = $commandTester->execute([
            CreateTeamCommand::ARGUMENT_TEAM_NAME => $name
        ]);
        $this->assertSame(Command::SUCCESS, $r);
    }

    public function testNotWorkingWithExistingName(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find(CreateTeamCommand::COMMAND_NAME);
        $commandTester = new CommandTester($command);

        $name = "team-duplicate" . (new \DateTime())->format("cu");

        // Loop twice to avoid copy/paste in second variable $r
        for($i = 1 ; $i <= 2 ; $i++) {
            $r = $commandTester->execute([
                CreateTeamCommand::ARGUMENT_TEAM_NAME => $name
            ]);
        }
        $this->assertSame(Command::FAILURE, $r);
    }
}