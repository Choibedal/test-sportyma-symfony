<?php

namespace App\Tests\UseCase;


use App\Domain\Entity\Player;
use App\Domain\Entity\Team;
use App\Infrastructure\Symfony\Command\AddPlayerToTeamCommand;
use App\UseCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class AddPlayerToTeamTest extends KernelTestCase
{

    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find(AddPlayerToTeamCommand::COMMAND_NAME);
        $commandTester = new CommandTester($command);

        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $player = $entityManager->getRepository(Player::class)->findOneBy(["team" => null]);
        $this->assertNotNull($player, "No player found in database. Rerun all tests to revalidate or create a player without a team.");
        $team = $entityManager->getRepository(Team::class)->findOneBy([]);
        $this->assertNotNull($team, "No team found. Rerun all tests to revalidate or create a team.");

        $r = $commandTester->execute([
            AddPlayerToTeamCommand::ARGUMENT_PLAYER_ID => $player->getId(),
            AddPlayerToTeamCommand::ARGUMENT_TEAM_ID =>  $team->getId()
        ]);
        $this->assertSame(Command::SUCCESS, $r);
    }

}