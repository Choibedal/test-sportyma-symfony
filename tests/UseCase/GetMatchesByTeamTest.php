<?php

namespace App\Tests\UseCase;

use App\Domain\Entity\SportsMatch;
use App\Infrastructure\Symfony\Command\GetMatchesCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class GetMatchesByTeamTest extends KernelTestCase
{

    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        /** @var SportsMatch $match */
        $match  = $entityManager->getRepository(SportsMatch::class)->findOneBy([]);
        if(null === $match) {
            return ;
        }

        $team = $match->getHostTeam();
        $command = $application->find(GetMatchesCommand::COMMAND_NAME);
        $commandTester = new CommandTester($command);
        $r = $commandTester->execute([
            GetMatchesCommand::ARGUMENT_TEAM_ID => $team->getId()
        ]);
        $this->assertSame(Command::SUCCESS, $r);
    }
}