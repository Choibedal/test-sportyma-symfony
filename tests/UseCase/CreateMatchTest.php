<?php

namespace App\Tests\UseCase;

use App\Domain\Entity\Team;
use App\Infrastructure\Symfony\Command\CreateMatchCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

class CreateMatchTest extends KernelTestCase
{
    public function testExecute(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $teams = $entityManager->getRepository(Team::class)->findBy([],[],2);
        $this->assertGreaterThanOrEqual(2, count($teams), "Not enough teams to run " . get_class());

        $command = $application->find(CreateMatchCommand::COMMAND_NAME);
        $commandTester = new CommandTester($command);
        $r = $commandTester->execute([
            CreateMatchCommand::ARGUMENT_MATCH_NAME => 'test-match',
            CreateMatchCommand::ARGUMENT_HOST_TEAM_ID => $teams[0]->getId(),
            CreateMatchCommand::ARGUMENT_VISITOR_TEAM_ID => $teams[1]->getId(),
        ]);
        $this->assertSame(Command::SUCCESS, $r);
    }

}