<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230202161207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add team entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE team (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id));");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_C4E0A61F5E237E06 ON team (name);");
        $this->addSql("COMMENT ON COLUMN team.id IS '(DC2Type:uuid)'");

    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE TEAM');
        $this->addSql('DROP INDEX UNIQ_C4E0A61F5E237E06');

    }
}
