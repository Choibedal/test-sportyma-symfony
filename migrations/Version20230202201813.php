<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230202201813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sports_match (id UUID NOT NULL, host_team_id UUID DEFAULT NULL, visitor_team_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9CA1AED01E90F49F ON sports_match (host_team_id)');
        $this->addSql('CREATE INDEX IDX_9CA1AED0EB7F4866 ON sports_match (visitor_team_id)');
        $this->addSql('COMMENT ON COLUMN sports_match.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN sports_match.host_team_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN sports_match.visitor_team_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE sports_match ADD CONSTRAINT FK_9CA1AED01E90F49F FOREIGN KEY (host_team_id) REFERENCES team (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sports_match ADD CONSTRAINT FK_9CA1AED0EB7F4866 FOREIGN KEY (visitor_team_id) REFERENCES team (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE sports_match DROP CONSTRAINT FK_9CA1AED01E90F49F');
        $this->addSql('ALTER TABLE sports_match DROP CONSTRAINT FK_9CA1AED0EB7F4866');
        $this->addSql('DROP TABLE sports_match');
    }
}
