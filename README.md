# Test Symfony LVL 2

## Installation

### Requirements

- >= PHP 8.1
- [Symfony Cli](https://symfony.com/download)
- [Docker >= 20](https://www.docker.com/)
- [Docker Compose >= 1.29](https://docs.docker.com/compose/overview/)

### Installation

```bash
git clone https://sportyma_jzirnheld@bitbucket.org/sportyma/test_symfony_lvl_2.git
make install
# Take a cup of coffee... ;)
```

## Running

### Start

After installation execute a simple : ```make start ``` and see information.
Once started, apply recent migrations : ```php bin/console d:m:m```.
### Stop

When you want stop stack, use ```make stop```

### More

More commands are available with ```make ```

## Déroulement du projet

### Prépatation (15 minutes)
- Analyse du squelette de base 
- Installation des dépendances manquantes au projet (pgsql)
- Commit 0 de création du projet sur Gitlab

### Commit 1 : CreateTeam (1h15)
- J'ai dû assimiler la notation des entités en XML (j'utilise généralement les annotations) et une architecture nouvelle, ce qui comme dans tout projet existant prend un peu de temps. Une fois le fonctionnement acquis, le développement est allé relativement sans encombres


### Commit 2 : GetTeams (15 minutes)
- M'appuyer sur les tests existants m'a permis d'aller plutôt vite sur cette partie

### Commit 3 : AddPLayerToTeam (1h15)
- Un petit souci avec le cache m'a fait perdre un bon quart d'heure sur cet exercice. 
- J'ai aussi tenté, comme je pensais l'avoir compris, de ne pas utiliser les ArrayCollection de Doctrine, mais cela ne rajoutait que de la complexité au code. Plus tard, j'ai reçu la confirmation que les collections étaient bien autorisées.

### Commit 4 : CreateMatch (1h15) 
- UN peu de temps perdu sur une ou deux innattentions au niveau du repository m'ont ralenties. 
- Correction des tests qui se basaient sur des IDs en dur que je n'avais pas remplacés lors de mes tests en local

### Commit 5 : GetMatches + filtre par équipe (15 minutes)
- Maintenant que l'archi était maitrisée, cette partie s'est déroulée sans soucis

### Cleanup + Test sur BDD vierge + Readme (30 minutes)
- Dernier commit en nettoyant le code (imports inutilisés, typage plus précis) 
- Vérification de non-utilisation d'IDs en local sur BDD vierge
- Ecriture du readme.

## Conclusion

Un projet bien complet que j'aurais aimé réussir en un peu moins de temps (4h15 + finalisation au lieu de 4h en tout), la faute peut-être au fait que j'avais laissé Symfony quelques mois de côté ! Mais c'est comme le vélo, ça revient vite ! 

